<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package smartco
 */
/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if (post_password_required()) {
    return;
}
// You can start editing here -- including this comment!
if (have_comments()) :
    ?>
    <!--Comments Area-->
    <div id="comments">
        <div class="comments-inner-wrap">
        <h3>
                <?php
				$smartco_comment_count = get_comments_number();
				if ('1' === $smartco_comment_count) {
					printf(
							/* translators: 1: title. */
							esc_html__('1 Comment', 'smartco')
					);
				} else {
					printf(// WPCS: XSS OK.
							/* translators: 1: comment count number, 2: title. */
							esc_html(_nx('%1$s Comment', '%1$s Comments ', $smartco_comment_count, 'comments title', 'smartco'), 'smartco'), number_format_i18n($smartco_comment_count)
					);
				}
				?>
        </h3>

        <?php
        // You can start editing here -- including this comment!
        if (have_comments()) :
            ?>
            <!-- .comments-title -->
            <?php the_comments_navigation(); ?>
            <ul class="commentlist">
                <?php
                wp_list_comments(array(
                    'style' => 'ul',
                    'callback' => 'smartco_comments',
                    'short_ping' => true,
                ));
                ?>
            </ul><!-- .comment-list -->
            <?php
            the_comments_navigation();
            // If comments are closed and there are comments, let's leave a little note, shall we?
            if (!comments_open()) :
                ?>
                <p class="no-comments"><?php esc_html_e('Comments are closed.', 'smartco'); ?></p>
                <?php
            endif;
        endif; // Check for have_comments().
        ?>
    </div>
</div>
    <?php
//You can start editing here -- including this comment!
endif;

?>
        <div class="row">
            <?php
      
        $user = wp_get_current_user();
        $smartco_user_identity = $user->display_name;
        $req = get_option('require_name_email');
        $aria_req = $req ? " aria-required='true'" : '';



        $formargs = array(
            'id_form' => 'comment-form',
            'id_submit' => 'post-comment',
            'class_submit' => 'btn btn-transparent',
            'class_form' => 'form-default',
            'title_reply' => '<h3>' . esc_html__('Leave a Reply', 'smartco') . '</h3>',
            'title_reply_to' => esc_html__('Leave a Reply to %s', 'smartco'),
            'cancel_reply_link' => esc_html__('Cancel Reply', 'smartco'),
            'label_submit' => esc_html__('Post Comment', 'smartco'),
            'submit_button' => '<div class="form-group"><button type="submit" name="%1$s" id="%2$s" class="%3$s">%4$s</button></div>',
            'comment_field' => '<div class="row clearfix"><div class="col-lg-12 col-md-12 col-sm-12 form-group"><textarea placeholder="' . esc_attr__('Your Comment..', 'smartco') . '"  class="form-control commentfield-big" id="comment" name="comment" cols="45" rows="8" aria-required="true">' .
            '</textarea></div></div>',
            'must_log_in' => '<div>' .
            sprintf(
                    wp_kses(__('You must be <a href="%s">logged in</a> to post a comment.', 'smartco'), array('a' => array('href' => array()))), wp_login_url(apply_filters('the_permalink', esc_url(get_permalink())))
            ) . '</div>',
            'logged_in_as' => '<div class="logged-in-as">' .
            sprintf(
                    wp_kses(__('Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="%4$s">Log out?</a>', 'smartco'), array('a' => array('href' => array()))), esc_url(admin_url('profile.php')), $smartco_user_identity, wp_logout_url(apply_filters('the_permalink', esc_url(get_permalink()))), esc_attr__('Log out of this account', 'smartco')
            ) . '</div>',
            'comment_notes_before' => '<p>' .
            esc_html__('Your email address will not be published.', 'smartco') . ( $req ? '<span class="required">*</span>' : '' ) .
            '</p>',
            'comment_notes_after' => '',
            'fields' => apply_filters('comment_form_default_fields', array(
                'author' =>
                '<div class="form-group">'
                . '<input id="author"  class="form-control" name="author" placeholder="' . esc_attr__('Name *', 'smartco') . '" type="text" value="' . esc_attr($commenter['comment_author']) .
                '" size="30"' . $aria_req . ' /></div>',
                'email' =>
                '<div class="form-group">'
                . '<input id="email" name="email"  class="form-control" type="text"  placeholder="' . esc_attr__('E-mail *', 'smartco') . '" value="' . esc_attr($commenter['comment_author_email']) .
                '" size="30"' . $aria_req . ' /></div>',
                    )
            )
        );


        ?>
        <div class="comment-reply-form col-xl-12 col-lg-12 col-md-12 col-sm-12">
        <?php
        comment_form($formargs);
        ?>
        </div>
    </div>
    <?php 
