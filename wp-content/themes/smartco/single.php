<?php 
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Smartco
 */
get_header();
global $smartco_options;
$smartco_blog_header_url = isset($smartco_options['smartco_blog_header_image']['url']) ? $smartco_options['smartco_blog_header_image']['url'] : '';
$smartco_blog_title = isset($smartco_options['smartco_blog_title']) ? $smartco_options['smartco_blog_title'] : '';
global $post;
$display_name = get_the_author_meta('display_name', $post->post_author);
$user_avatar = get_avatar($post->post_author, 175);
?>

    <!-- =========single-blog header area=========== -->
    <section>
        <div class="single-blog-header-area image_background" data-image-src="<?php echo esc_url($smartco_blog_header_url);?>">
            <div class="intro-text" data-aos="fade-up" data-aos-duration="1200" data-aos-easing="ease-in-sine" data-aos-anchor-placement="top-bottom">
                <div class="client-img">
                <?php echo wp_kses_post($user_avatar); ?>
                </div>
                <h3><?php esc_html_e('Published By:','smartco');?> <?php echo wp_kses_post(ucfirst($display_name)); ?> <br><?php the_time( get_option( 'date_format' ) ); ?></h3>
            </div>
        </div>
    </section>
<!-- =========single-blog area=========== -->
    <section>
        <div class="container">
            <div class="single-blog-area">
                <div class="row">
               <?php if (is_active_sidebar('sidebar')) { ?>
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
               <?php }else{ ?>
                    <div class="col-sm-12 margin-auto">
               <?php } ?>
                    <?php
                    while (have_posts()) :
                        the_post();    
                        ?>                    
                        <h1 class="single-blog-title"><?php the_title();?></h1>
                        <div class="single-blog-content">
                            <div class="blog-post-image">
                              <?php 
                                    if (has_post_thumbnail()) { 
                                        echo the_post_thumbnail();
                                    }
                                ?>
                            </div>
                                <?php
                                the_content();
                                ?>
                                <div class="tagcloud">
                                <?php echo get_the_tag_list(); ?>
                                </div>
                                <?php
                                wp_link_pages(array(
                                    'before' => '<div class="page-links">' . esc_html__('Pages:', 'smartco'),
                                    'after' => '</div>',
                                ));
                                ?>
                        </div>
                    <?php
                         // If comments are open or we have at least one comment, load up the comment template.
                        if (comments_open() || get_comments_number()) :
                            comments_template();
                        endif;
                     endwhile; // End of the loop
                     ?>
                    </div>
                  
                    <?php
                    if (is_active_sidebar('sidebar')) {
                        get_sidebar('sidebar');
                    }
                    ?>  
                </div>
            </div>
        </div>
    </section>
   


<?php get_footer()?>