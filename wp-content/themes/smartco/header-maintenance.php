
<!doctype html>
<html <?php language_attributes(); ?>>

    <head>
      
        <!-- Required meta tags -->
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <meta name=author content="Enventer">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php
        if (function_exists('has_site_icon') && has_site_icon()) { // since 4.3.0
            wp_site_icon();
        }
        ?>
     
        <?php wp_head(); ?>
    </head>

<body <?php body_class(); ?>>