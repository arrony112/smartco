<?php 
get_header()
?>
    <section>
        <div class="error-page">
            <!-- 404-area-->
            <div class='is-animate style1'>
                <div><span><?php esc_html_e('4','smartco');?></span></div>
                <div><span><?php esc_html_e('0','smartco');?></span></div>
                <div><span><?php esc_html_e('4','smartco');?></span></div>
            </div>
            <div class="defult-text">
                <h1><?php esc_html_e("Opps! That page couldn't be found.","smartco")?></h1>
                <a href="<?php echo esc_url(home_url('/')); ?>"><?php esc_html_e('take me back','smartco')?></a>
            </div>
        </div>
    </section>

<?php get_footer()?>