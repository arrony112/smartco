<?php 
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Smartco
 */
get_header();

global $smartco_options;
$smartco_blog_header_url = isset($smartco_options['smartco_blog_header_image']['url']) ? $smartco_options['smartco_blog_header_image']['url'] : '';
$smartco_blog_title = isset($smartco_options['smartco_blog_title']) ? $smartco_options['smartco_blog_title'] : '';
$smartco_blog_subtitle = isset($smartco_options['smartco_blog_subtitle']) ? $smartco_options['smartco_blog_subtitle'] : '';
?>
    <section>
        <div class="blog-header-area image_background" data-image-src="<?php echo esc_url($smartco_blog_header_url);?>">
            <div class="intro-text">
                <?php if(!empty($smartco_blog_title)){?>
                <h1><?php echo wp_kses_post($smartco_blog_title);?></h1>
                <?php }else{ ?>
                    <h1><?php the_title();?></h1>
               <?php } ?>
               <?php if(!empty($smartco_blog_subtitle)){?>
                <h3><?php echo wp_kses_post($smartco_blog_subtitle);?></h3>
                <?php } ?>
            </div>
        </div>
    </section>
    <section>
        <div class="blog-area">
            <div class="container">
                <div class="row">
                     <?php
                    if (have_posts()) :
                        /* Start the Loop */
                        while (have_posts()) :
                            the_post();
                            /*
                             * Include the Post-Type-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                             */
                            get_template_part('template-parts/content', get_post_format());
                        endwhile;
                    else :
                        get_template_part('template-parts/content', 'none');
                    endif;
                    ?>                
                </div>
            </div>
            <div class="center">
                <?php
                the_posts_pagination(array(
                        'mid_size' => 2,
                        'prev_text' => '<span class="fas fa-angle-left"></span>',
                        'next_text' => '<span class="fas fa-angle-right"></span>'
                    ));
                ?>
            </div>
        </div>
    </section>
    <!-- .section -->

<?php get_footer()?>