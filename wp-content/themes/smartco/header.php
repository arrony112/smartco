
<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Smartco
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Required meta tags -->
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name=author content="Enventer">
    <?php
    if (function_exists('has_site_icon') && has_site_icon()) { // since 4.3.0
        wp_site_icon();
    }
    ?>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <div id='preloader'>
        <div id='status'>
            <img src='<?php echo SMARTCO_IMG_URL;?>/loading.gif' alt='<?php esc_attr_e('LOADING....!!!!!','smartco');?>' />
        </div>
    </div>
    <!--=========== Main Header area ===============-->
    <header id="home">
        <?php if(is_404()){ ?>
            <div class="main-navigation sticky-header sticker error-page-header">
        <?php }else{ ?>
            <div class="main-navigation sticky-header sticker">
        <?php }  ?>
            <div class="container">
                <div class="row">
                    <!-- logo-area-->
                    <div class="col-xl-2 col-lg-2 col-md-2">
                        <div class="logo-area">
                            <?php if (function_exists('get_custom_logo') && has_custom_logo()) {
                                the_custom_logo();
                            }else{ ?>
                            <a href="<?php echo esc_url(home_url('/')); ?>">
                                <img src="<?php echo esc_url(SMARTCO_IMG_URL . 'logo.png') ?>" alt="logo">
                            </a>
                           <?php } ?>

                        </div>
                    </div>
                    <!-- mainmenu-area-->
                    <div class="col-xl-10 col-lg-10 col-md-10">
                        <div class="main-menu f-right">
                            <nav id="mobile-menu">
                            <?php
                                if (has_nav_menu('primary')) {
                                    wp_nav_menu(array(
                                        'theme_location' => 'primary',
                                        'depth' => 3, // 1 = no dropdowns, 2 = with dropdowns.
                                        'menu_class' => 'menu-area',
                                        'menu_id' => 'mobile-menu',
                                    ));
                                }else{
                                    wp_nav_menu(array(
                                        'depth' => 3, // 1 = no dropdowns, 2 = with dropdowns.
                                        'menu_class' => 'menu-area',
                                        'menu_id' => 'mobile-menu',
                                    ));       
                                }
                            ?>
                            </nav>
                        </div>
                        <!-- mobile menu-->
                        <div class="mobile-menu"></div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- =========Header Image area=========== -->