<?php
/**
 * The template for displaying search results pages
 *
 *
 * @package Smartco
 */

get_header(); ?>
<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>
<div class="top-header">
    <form role="search" method="get" class="input-group" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <input type="search"  class="form-control" id="inlineFormInputGroupUsername" placeholder="<?php esc_attr_e( 'blog search','smartco' ); ?>" value="<?php echo get_search_query(); ?>" name="s"  required="">
        <div class="input-group-prepend">
        <button type="submit" class="input-group-text"><i class="fas fa-search"></i></button>
        </div>
    </form>
</div>
