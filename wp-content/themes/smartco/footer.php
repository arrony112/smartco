
<?php
global $smartco_options;
$smartco_footer_image_link = isset($smartco_options['smartco_footer_image']['url']) ? $smartco_options['smartco_footer_image']['url'] : '';
$smartco_footer_widget = isset($smartco_options['smartco_footer_widget']) ? $smartco_options['smartco_footer_widget'] : 0;
$smartco_copyright_text = isset($smartco_options['smartco_copyright_text']) ? $smartco_options['smartco_copyright_text'] : '© 2019 All Right Reserved By <a href="#">Smartco</a>';

$smartco_page_style =  get_post_meta(get_the_id(), 'smartco_page_style'); 
?>
<!-- =========Footer area=========== -->
<?php if(isset($smartco_page_style[0]) && $smartco_page_style[0] == 'two'){ ?>  
<footer class="footer-area-2 image_background" data-image-src="<?php echo esc_url($smartco_footer_image_link);?>">
<?php }else{ ?>
<footer class="footer-area image_background" data-image-src="<?php echo esc_url($smartco_footer_image_link);?>">
<?php } ?>
    <?php if($smartco_footer_widget == 1){?>
    <!-- footer bottom -->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 margin-bottom-r">
                    <?php
                    if (is_active_sidebar('footer_column_1')) {
                        dynamic_sidebar('footer_column_1');
                    }
                    ?>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-6 margin-bottom-r margin-top-small">
                    <?php
                    if (is_active_sidebar('footer_column_2')) {
                        dynamic_sidebar('footer_column_2');
                    }
                    ?>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 margin-bottom-r margin-top-small">
                    <?php
                    if (is_active_sidebar('footer_column_3')) {
                        dynamic_sidebar('footer_column_3');
                    }
                    ?>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 margin-top-small">
                    <?php
                    if (is_active_sidebar('footer_column_4')) {
                        dynamic_sidebar('footer_column_4');
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>

    <!-- footer bottom -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <!-- Copyright -->
                    <div class="copyright">
                        <p><?php echo wp_kses_post($smartco_copyright_text);?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/ End footer -->

<?php wp_footer(); ?>
</body>

</html>