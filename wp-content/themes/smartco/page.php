<?php 
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Smartco
 */
get_header();
$smartco_header_image_link =  get_the_post_thumbnail_url(get_the_ID(),'full'); 
$smartco_title =  get_post_meta(get_the_id(), 'smartco_title', true); 
$smartco_subtitle =  get_post_meta(get_the_id(), 'smartco_subtitle', true); 
$smartco_button_text =  get_post_meta(get_the_id(), 'smartco_button_text', true); 
$smartco_button_link =  get_post_meta(get_the_id(), 'smartco_button_link', true); 
$smartco_page_style =  get_post_meta(get_the_id(), 'smartco_page_style'); 
?>
    <section>
        <?php if(is_front_page()){?>
            <?php if(isset($smartco_page_style[0]) && $smartco_page_style[0] == 'two'){ ?>  
                <div class="header-image-area-2 image_background" data-image-src="<?php echo esc_url($smartco_header_image_link);?>">
            <?php }else{ ?>
                <div class="header-image-area image_background" data-image-src="<?php echo esc_url($smartco_header_image_link);?>">
          <?php  } ?>
        <?php }else{ ?>
            <?php if(isset($smartco_page_style[0]) && $smartco_page_style[0] == 'two'){ ?>  
                <div class="header-image-area-2 image_background" data-image-src="<?php echo esc_url($smartco_header_image_link);?>">
            <?php }else{ ?>
            <div class="blog-header-area image_background" data-image-src="<?php echo esc_url($smartco_header_image_link);?>">
            <?php  } ?>
        <?php } ?>
            <div class="intro-text">
                <?php if(!empty($smartco_title)){?>
                <h1><?php echo wp_kses_post($smartco_title);?></h1>
                <?php }else{ ?>
                    <h1><?php the_title();?></h1>
               <?php } ?>
                <h3><?php echo wp_kses_post($smartco_subtitle);?></h3>
                <?php if(!empty($smartco_button_link)){?>
                <div class="learnmore">
                    <a href="<?php echo esc_url($smartco_button_link);?>" class="skill-btn"><?php echo esc_html($smartco_button_text);?></a>
                </div>
                <?php } ?>
            </div>
            <?php if(isset($smartco_page_style[0]) && $smartco_page_style[0] == 'two'){ ?>
                <!-- particles.js container -->
                <div id="particles-js"></div>
            <?php } ?>
        </div>
    </section>
    <!--Blog Section-->
        <div class="page-area">
            <div class="container">
                <?php
                    while ( have_posts() ) : the_post();

                    ?>
                    <div  id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                        <?php
                        the_content();

                        wp_link_pages( array(
                            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'smartco' ),
                            'after'  => '</div>',
                        ) );
                        ?>

                    </div>
                <?php

                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) :
                            comments_template();
                        endif;

                    endwhile; // End of the loop.
                    ?>               
                </div>
        </div>
       
    <!-- .section -->

<?php get_footer()?>