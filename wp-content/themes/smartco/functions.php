<?php
/**
 * smartco functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package smartco
 */
defined('SMARTCO_THEME_URI') or define('SMARTCO_THEME_URI', get_template_directory_uri());
define('SMARTCO_CSS_URL', SMARTCO_THEME_URI . '/assets/css');
define('SMARTCO_JS_URL', SMARTCO_THEME_URI . '/assets/js');
define('SMARTCO_FONTS_URL', SMARTCO_THEME_URI . '/assets/fonts');
define('SMARTCO_IMG_URL', SMARTCO_THEME_URI . '/assets/images/');
define('SMARTCO_THEME_DIR', get_template_directory());
define('SMARTCO_FREAMWORK_DIRECTORY', SMARTCO_THEME_DIR . '/framework/');

/** Redux framework configuration */
require_once(SMARTCO_FREAMWORK_DIRECTORY . "redux.config.php");

/** Enable support TGM features. */
require_once(SMARTCO_FREAMWORK_DIRECTORY . "class-tgm-plugin-activation.php");
require_once(SMARTCO_FREAMWORK_DIRECTORY . "config-tgm.php");

if (!function_exists('smartco_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function smartco_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on smartco, use a find and replace
         * to change 'smartco' to the name of your theme in all the template files.
         */
        load_theme_textdomain('smartco', get_template_directory() . '/languages');
        add_editor_style(SMARTCO_CSS_URL . '/editor-style.css');
        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');
        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');
        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');
        // This theme uses wp_nav_menu() in one location.~
        register_nav_menus(array(
            'primary' => esc_html__('Primary', 'smartco'),
        ));

        add_theme_support('post-formats', array(
            'aside',
            'image',
            'gallery',
            'audio',
            'video'
        ));
        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));
        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('smartco_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));
        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');
        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');
        // Add support for Block Styles.
        add_theme_support('wp-block-styles');
        // Add support for full and wide align images.
        add_theme_support('align-wide');
        // Add support for editor styles.
        add_theme_support('editor-styles');
        // Add custom editor font sizes.
        add_theme_support(
                'editor-font-sizes', array(
            array(
                'name' => esc_html__('Small', 'smartco'),
                'shortName' => esc_html__('S', 'smartco'),
                'size' => 19.5,
                'slug' => 'small',
            ),
            array(
                'name' => esc_html__('Normal', 'smartco'),
                'shortName' => esc_html__('M', 'smartco'),
                'size' => 22,
                'slug' => 'normal',
            ),
            array(
                'name' => esc_html__('Large', 'smartco'),
                'shortName' => esc_html__('L', 'smartco'),
                'size' => 36.5,
                'slug' => 'large',
            ),
            array(
                'name' => esc_html__('Huge', 'smartco'),
                'shortName' => esc_html__('XL', 'smartco'),
                'size' => 49.5,
                'slug' => 'huge',
            ),
                )
        );

// Editor color palette.
        add_theme_support('editor-color-palette', array(
            array(
                'name' => esc_html__('strong yellow', 'smartco'),
                'slug' => 'strong-yellow',
                'color' => '#f7bd00',
            ),
            array(
                'name' => esc_html__('strong white', 'smartco'),
                'slug' => 'strong-white',
                'color' => '#fff',
            ),
            array(
                'name' => esc_html__('light black', 'smartco'),
                'slug' => 'light-black',
                'color' => '#242424',
            ),
            array(
                'name' => esc_html__('very light gray', 'smartco'),
                'slug' => 'very-light-gray',
                'color' => '#797979',
            ),
            array(
                'name' => esc_html__('very dark black', 'smartco'),
                'slug' => 'very-dark-black',
                'color' => '#000000',
            ),
        ));
        // Add support for responsive embedded content.
        add_theme_support('responsive-embeds');
        //Add custom thumb size
        set_post_thumbnail_size(870, 483, false);
        add_image_size('smartco_blog_post_featured_image', 770, 462, true);
        add_image_size('smartco_sidebar_post_image', 75, 70, true);
        add_image_size('smartco-serives-single', 770, 441, true);
        add_image_size('smartco-blog-carousel', 370, 411, true);
        add_image_size('smartco-blog-sidebar', 75, 70, true);
    }

endif;
add_action('after_setup_theme', 'smartco_setup');







function smartco_scripts() {
    wp_enqueue_style('bootstrap-css', SMARTCO_CSS_URL . '/bootstrap.min.css', '', null);
    wp_enqueue_style('fontawesome-css', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css', '', null);
    wp_enqueue_style('meanmenu', SMARTCO_CSS_URL . '/meanmenu.css', '', null);
    wp_enqueue_style('aos-css', SMARTCO_CSS_URL . '/aos.min.css', '', null);
    wp_enqueue_style('owl-carousel', SMARTCO_CSS_URL . '/owl.carousel.min.css', '', null);
    wp_enqueue_style('owl-carousel-default', SMARTCO_CSS_URL . '/owl.theme.default.min.css', '', null);
    wp_enqueue_style('animations', SMARTCO_CSS_URL . '/animations.css', array('elementor-animations'), null);
    wp_enqueue_style('smartco-style', get_stylesheet_uri());
    wp_enqueue_style('responsive', SMARTCO_CSS_URL . '/responsive.css', '', null);

    wp_enqueue_script('jquery-ui-core');
    wp_enqueue_script('popper', SMARTCO_JS_URL . '/popper.min.js', array('jquery'), '', true);
    wp_enqueue_script('bootstrap-js', SMARTCO_JS_URL . '/bootstrap.min.js', array('jquery'), '', true);
    wp_enqueue_script('waypoints', SMARTCO_JS_URL . '/waypoints.min.js', array('jquery'), '', true);
    wp_enqueue_script('counterup', SMARTCO_JS_URL . '/counterup.min.js', array('jquery'), '', true);
    wp_enqueue_script('meanmenu-js', SMARTCO_JS_URL . '/meanmenu.min.js', array('jquery'), '', true);
    wp_enqueue_script('jquery-downCount', SMARTCO_JS_URL . '/jquery.downCount.js', array('jquery'), '', true);
    wp_enqueue_script('aos-js', SMARTCO_JS_URL . '/aos.min.js', array('jquery'), '', true);
    wp_enqueue_script('owl-carousel-js', SMARTCO_JS_URL . '/owl.carousel.min.js', array('jquery'), '', true);
    wp_enqueue_script('scrollUp', SMARTCO_JS_URL . '/scrollUp.js', array('jquery'), '', true);
    wp_enqueue_script('magnific-popup', SMARTCO_JS_URL . '/magnific-popup.min.js', array('jquery'), '', true);

    $smartco_page_style =  get_post_meta(get_the_id(), 'smartco_page_style'); 
    if(isset($smartco_page_style[0]) && $smartco_page_style[0] == 'two'){
    wp_enqueue_script('particles', SMARTCO_JS_URL . '/particles.js', array('jquery'), '', true);
    wp_enqueue_script('app', SMARTCO_JS_URL . '/app.js', array('jquery'), '', true);
    }

    wp_enqueue_script('smartco-main', SMARTCO_JS_URL . '/main.js', array('jquery'), '', true);


    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }


}

add_action('wp_enqueue_scripts', 'smartco_scripts');


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function smartco_content_width() {
// This variable is intended to be overruled from themes.
// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('smartco_content_width', 240);
}

add_action('after_setup_theme', 'smartco_content_width', 0);



/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function smartco_widgets_init() {

    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'smartco'),
        'id' => 'sidebar',
        'description' => esc_html__('Add widgets here.', 'smartco'),
        'before_widget' => '<div id="%1$s" class="sidebar-block %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="sidebar-title"><p>',
        'after_title' => '</p></div>',
    ));

    global $smartco_options;
    $smartco_footer_widget = isset($smartco_options['smartco_footer_widget']) ? $smartco_options['smartco_footer_widget'] : 0;
    if($smartco_footer_widget == 1){

        register_sidebar(
                array(
                    'name' => esc_html__('Footer Column 1', 'smartco'),
                    'id' => 'footer_column_1',
                    'description' => esc_html__('Widgets in this area will be shown on footer widget.', 'smartco'),
                    'before_widget' => '<div id="%1$s" class="widget single-widget border-right %2$s">',
                    'after_widget' => '</div>',
                    'before_title' => '<h2>',
                    'after_title' => '</h2>',
        ));

        register_sidebar(
                array(
                    'name' => esc_html__('Footer Column 2', 'smartco'),
                    'id' => 'footer_column_2',
                    'description' => esc_html__('Widgets in this area will be shown on footer widget.', 'smartco'),
                    'before_widget' => '<div id="%1$s" class="widget single-widget %2$s">',
                    'after_widget' => '</div>',
                    'before_title' => '<h2>',
                    'after_title' => '</h2>',
        ));

        register_sidebar(
                array(
                    'name' => esc_html__('Footer Column 3', 'smartco'),
                    'id' => 'footer_column_3',
                    'description' => esc_html__('Widgets in this area will be shown on footer widget.', 'smartco'),
                    'before_widget' => '<div id="%1$s" class="widget single-widget %2$s">',
                    'after_widget' => '</div>',
                    'before_title' => '<h2>',
                    'after_title' => '</h2>',
        ));

        register_sidebar(
                array(
                    'name' => esc_html__('Footer Column 4', 'smartco'),
                    'id' => 'footer_column_4',
                    'description' => esc_html__('Widgets in this area will be shown on footer widget.', 'smartco'),
                    'before_widget' => '<div id="%1$s" class="widget single-widget %2$s">',
                    'after_widget' => '</div>',
                    'before_title' => '<h2>',
                    'after_title' => '</h2>',
        ));
    }
}

add_action('widgets_init', 'smartco_widgets_init');

if (!function_exists('smartco_categories')) :

    /**
     * Prints HTML with meta information for the categories, tags and comments.
     */
    function smartco_categories() {
        // Hide category and tag text for pages.
        if ('post' === get_post_type()) {
            /* translators: used between list items, there is a space after the comma */
            $categories_list = get_the_category_list(esc_html__(', ', 'smartco'));
            if ($categories_list) {
                /* translators: 1: list of categories. */
                printf('<span>' . esc_html('%1$s') . '</span>', $categories_list); // WPCS: XSS OK.
            }
        }
    }

endif;



// smartco_comments
function smartco_comments($comment, $args, $depth) {

        $tag = 'li';
    ?>
    <<?php echo esc_html($tag); ?> <?php comment_class(empty($args['has_children']) ? 'comment-box' : 'parent comment-box' ); ?> id="comment-<?php comment_ID() ?>">
        <div id="div-comment-<?php comment_ID() ?>" class="comment-box"><?php
    ?>
            <?php if($comment->comment_type!='trackback' && $comment->comment_type!='pingback' ){ ?>
            <div class="comment-author">
                <?php print get_avatar($comment, 70, null, null, array('class' => array())); ?>
            </div>
            <?php } ?>
            <div class="comment-body">
                <div class="commentator"><?php echo get_comment_author_link(); ?></div>
                <div class="comment-meta"><span><?php comment_time(get_option('date_format')); ?></span></div>
                <?php comment_text(); ?>
            </div>
            <div class="comment-abs">
                <?php
                comment_reply_link(array_merge($args, array(
                    'reply_text' => esc_html__('Reply ', 'smartco'),
                    'depth' => $depth,
                    'max_depth' => $args['max_depth']
                                )
                ));
                ?>
            </div>
        <?php
}