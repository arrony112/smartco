<?php 
/*
 * Template Name: Comming Soon
 */
get_header('maintenance'); 
global $smartco_options;

$twitter_url = isset($smartco_options['smartco_twitter_url']) ? $smartco_options['smartco_twitter_url'] : '';
$facebook_url = isset($smartco_options['smartco_facebook_url']) ? $smartco_options['smartco_facebook_url'] : '';
$reddit_url = isset($smartco_options['smartco_reddit_url']) ? $smartco_options['smartco_reddit_url'] : '';
$pinterest_url = isset($smartco_options['smartco_pinterest_url']) ? $smartco_options['smartco_pinterest_url'] : '';
$google_plus_url = isset($smartco_options['smartco_google_plus_url']) ? $smartco_options['smartco_google_plus_url'] : '';
$linkedin_url = isset($smartco_options['smartco_linkedin_url']) ? $smartco_options['smartco_linkedin_url'] : '';
?>
    <!--  Coming Soon Area
    ========================-->
    <div class="coming-soon-area">
        <div class="coming-soon-text">
           <!-- logo-area-->
           <?php if (function_exists('get_custom_logo') && has_custom_logo()) {
                    the_custom_logo();
                }else{ ?>
                <a href="<?php echo esc_url(home_url('/')); ?>">
                    <img src="<?php echo esc_url(SMARTCO_IMG_URL . 'logo.png') ?>" alt="logo">
                </a>
            <?php } ?>
            <h1><?php the_title();?></h1>
            <!-- countdown-area-->
            <div id="clock">
                <ul class="countdown">
                    <li>
                        <span class="days"><?php esc_html_e('00','smartco');?></span>
                        <p class="days_ref"><?php esc_html_e('days','smartco');?></p>
                    </li>
                    <li>
                        <span class="hours"><?php esc_html_e('00','smartco');?></span>
                        <p class="hours_ref"><?php esc_html_e('hours','smartco');?></p>
                    </li>
                    <li>
                        <span class="minutes"><?php esc_html_e('00','smartco');?></span>
                        <p class="minutes_ref"><?php esc_html_e('minutes','smartco');?></p>
                    </li>
                    <li>
                        <span class="seconds last"><?php esc_html_e('00','smartco');?></span>
                        <p class="seconds_ref"><?php esc_html_e('seconds','smartco');?></p>
                    </li>
                </ul>
            </div>
        </div>
        <!-- social-area-->
        <div class="coming-share">
            <span><?php esc_html_e('follow us','smartco');?></span>
            <?php if(!empty($twitter_url)){?>
            <a href="<?php echo esc_url($twitter_url);?>"><i class="fab fa-twitter-square"></i></a>
            <?php } ?>
            <?php if(!empty($facebook_url)){?>
            <a href="<?php echo esc_url($facebook_url);?>"><i class="fab fa-facebook-square"></i></a>
            <?php } ?>
            <?php if(!empty($reddit_url)){?>
            <a href="<?php echo esc_url($reddit_url);?>"><i class="fab fa-reddit"></i></a>
            <?php } ?>
            <?php if(!empty($pinterest_url)){?>
            <a href="<?php echo esc_url($pinterest_url);?>"><i class="fab fa-pinterest"></i></a>
            <?php } ?>
            <?php if(!empty($google_plus_url)){?>
            <a href="<?php echo esc_url($google_plus_url);?>"><i class="fab fa-google-plus-square"></i></a>
            <?php } ?>
            <?php if(!empty($linkedin_url)){?>
            <a href="<?php echo esc_url($linkedin_url);?>"><i class="fab fa-linkedin"></i></a>
            <?php } ?>
        </div>
    </div>

  <?php  get_footer('maintenance'); ?>