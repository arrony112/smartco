<?php

class SmartcoContactWidget extends WP_Widget {

// Main constructor
    public function __construct() {
        parent::__construct(
                'smartco_custom_contact', __('Smartco Contact', 'smartco-core'), array(
            'customize_selective_refresh' => true,
                )
        );
    }

    public function form($instance) {

        $defaults = array(
            'title' => '',
            'subtitle' => '',
            'address' => '',
            'phone' => '',
            'email' => '',
        );

        extract(wp_parse_args((array) $instance, $defaults));
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Widget Title', 'smartco-core'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('subtitle')); ?>"><?php _e('Subtitle', 'smartco-core'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('subtitle')); ?>" name="<?php echo esc_attr($this->get_field_name('subtitle')); ?>" type="text" value="<?php echo esc_attr($subtitle); ?>" />
        </p>


        <p>
            <label for="<?php echo esc_attr($this->get_field_id('address')); ?>"><?php _e('Address', 'smartco-core'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('address')); ?>" name="<?php echo esc_attr($this->get_field_name('address')); ?>" type="text" value="<?php echo esc_attr($address); ?>" />
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('phone')); ?>"><?php _e('Phone', 'smartco-core'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('phone')); ?>" name="<?php echo esc_attr($this->get_field_name('phone')); ?>" type="text" value="<?php echo esc_attr($phone); ?>" />
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('email')); ?>"><?php _e('Email', 'smartco-core'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('email')); ?>" name="<?php echo esc_attr($this->get_field_name('email')); ?>" type="text" value="<?php echo esc_attr($email); ?>" />
        </p>


        <?php
    }

    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = isset($new_instance['title']) ? wp_strip_all_tags($new_instance['title']) : '';
        $instance['subtitle'] = isset($new_instance['subtitle']) ? wp_strip_all_tags($new_instance['subtitle']) : '';
        $instance['address'] = isset($new_instance['address']) ? wp_strip_all_tags($new_instance['address']) : '';
        $instance['phone'] = isset($new_instance['phone']) ? wp_strip_all_tags($new_instance['phone']) : '';
        $instance['email'] = isset($new_instance['email']) ? wp_strip_all_tags($new_instance['email']) : '';
        return $instance;
    }

    public function widget($args, $instance) {

        extract($args);
        $title = isset($instance['title']) ? apply_filters('widget_title', $instance['title']) : '';
        $subtitle = isset($instance['subtitle']) ? $instance['subtitle'] : '';
        $address = isset($instance['address']) ? $instance['address'] : '';
        $phone = isset($instance['phone']) ? $instance['phone'] : '';
        $email = isset($instance['email']) ? $instance['email'] : '';
        ?>
        <div class="single-widget">
            <?php if(!empty($title)){?>
            <h2><?php echo $title;?></h2>
            <?php } ?>
            <div class="our-address">
                <?php if(!empty($subtitle)){?>
                <p><?php echo $subtitle;?></p>
                <?php } ?>
                <ul class="list footer-w">
                <?php if(!empty($address)){?>
                <li><i class="fa fa-map-marker"></i><?php echo $address;?></li>
                <?php } ?>
                <?php if(!empty($phone)){?>
                <li><i class="fa fa-phone-square"></i><?php echo $phone;?></li>
                <?php } ?>
                <?php if(!empty($email)){?>
                <li><i class="fa fa-envelope"></i><a href="mailto:<?php echo $email;?>"><?php echo $email;?></a></li>
                <?php } ?>
                </ul>
            </div>
        </div>

        <?php
    }

}

function smartco_contact_widget() {
    register_widget('SmartcoContactWidget');
}

add_action('widgets_init', 'smartco_contact_widget');
?>