<?php

class SmartcoSocialLink extends WP_Widget {

// Main constructor
    public function __construct() {
        parent::__construct(
                'smartco_custom_social_link', __('Smartco Social Link', 'smartco-core'), array(
            'customize_selective_refresh' => true,
                )
        );
    }

    public function form($instance) {

        $defaults = array(
            'title' => '',
            'facebook' => '',
            'twitter' => '',
            'linkedin' => '',
            'behance' => '',
            'youtube' => '',
        );

        extract(wp_parse_args((array) $instance, $defaults));
        ?>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Widget Title', 'smartco-core'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('facebook')); ?>"><?php _e('Facebook Url', 'smartco-core'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('facebook')); ?>" name="<?php echo esc_attr($this->get_field_name('facebook')); ?>" type="text" value="<?php echo esc_attr($facebook); ?>" />
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('twitter')); ?>"><?php _e('Twitter Url', 'smartco-core'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('twitter')); ?>" name="<?php echo esc_attr($this->get_field_name('twitter')); ?>" type="text" value="<?php echo esc_attr($twitter); ?>" />
        </p>

        <p>
            <label for="<?php echo esc_attr($this->get_field_id('linkedin')); ?>"><?php _e('Linkedin Url', 'smartco-core'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('linkedin')); ?>" name="<?php echo esc_attr($this->get_field_name('linkedin')); ?>" type="text" value="<?php echo esc_attr($linkedin); ?>" />
        </p>


        <p>
            <label for="<?php echo esc_attr($this->get_field_id('behance')); ?>"><?php _e('Behance Url', 'smartco-core'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('behance')); ?>" name="<?php echo esc_attr($this->get_field_name('behance')); ?>" type="text" value="<?php echo esc_attr($behance); ?>" />
        </p>


        <p>
            <label for="<?php echo esc_attr($this->get_field_id('youtube')); ?>"><?php _e('Youtube Url', 'smartco-core'); ?></label>
            <input class="widefat" id="<?php echo esc_attr($this->get_field_id('youtube')); ?>" name="<?php echo esc_attr($this->get_field_name('youtube')); ?>" type="text" value="<?php echo esc_attr($youtube); ?>" />
        </p>


        <?php
    }

    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = isset($new_instance['title']) ? wp_strip_all_tags($new_instance['title']) : '';
        $instance['facebook'] = isset($new_instance['facebook']) ? wp_strip_all_tags($new_instance['facebook']) : '';
        $instance['twitter'] = isset($new_instance['twitter']) ? wp_strip_all_tags($new_instance['twitter']) : '';
        $instance['linkedin'] = isset($new_instance['linkedin']) ? wp_strip_all_tags($new_instance['linkedin']) : '';
        $instance['behance'] = isset($new_instance['behance']) ? wp_strip_all_tags($new_instance['behance']) : '';
        $instance['youtube'] = isset($new_instance['youtube']) ? wp_strip_all_tags($new_instance['youtube']) : '';
        return $instance;
    }

    public function widget($args, $instance) {

        extract($args);
        $title = isset($instance['title']) ? apply_filters('widget_title', $instance['title']) : '';
        $facebook = isset($instance['facebook']) ? $instance['facebook'] : '';
        $twitter = isset($instance['twitter']) ? $instance['twitter'] : '';
        $linkedin = isset($instance['linkedin']) ? $instance['linkedin'] : '';
        $behance = isset($instance['behance']) ? $instance['behance'] : '';
        $youtube = isset($instance['youtube']) ? $instance['youtube'] : '';
        ?>
        <?php if(!empty($title)){?>
        <h2><?php echo $title;?></h2>
        <?php } ?>
        <ul class="social">
            <?php if(!empty($facebook)){?>
            <li><a class="footer-socials" href="<?php echo esc_url($facebook); ?>"><i class="fab fa-facebook"></i></a></li>
            <?php } ?>
            <?php if(!empty($twitter)){?>
            <li><a class="footer-socials" href="<?php echo esc_url($twitter); ?>"><i class="fab fa-twitter"></i></a></li>
            <?php } ?>
            <?php if(!empty($linkedin)){?>
            <li><a class="footer-socials" href="<?php echo esc_url($linkedin); ?>"><i class="fab fa-linkedin"></i></a></li>
            <?php } ?>
            <?php if(!empty($behance)){?>
            <li><a class="footer-socials" href="<?php echo esc_url($behance); ?>"><i class="fab fa-behance"></i></a></li>
            <?php } ?>
            <?php if(!empty($youtube)){?>
            <li><a class="footer-socials" href="<?php echo esc_url($youtube); ?>"><i class="fab fa-youtube"></i></a></li>
            <?php } ?>
        </ul>

        <?php
    }

}

function smartco_social_network() {
    register_widget('SmartcoSocialLink');
}

add_action('widgets_init', 'smartco_social_network');
?>