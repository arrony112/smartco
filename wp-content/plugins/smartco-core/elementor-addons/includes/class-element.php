<?php

namespace Smartco;

class Element {

    public function __construct() {
       
        add_action('elementor/widgets/widgets_registered', array($this, 'widgets_registered'));
    }

    public function widgets_registered() {
      
        if (defined('SMARTCO_ELEMENTOR_PATH') && class_exists('Elementor\Widget_Base')) {
            require_once SMARTCO_ELEMENTOR_INCLUDES . '/widgets/smartco-service-box.php';
            require_once SMARTCO_ELEMENTOR_INCLUDES . '/widgets/smartco-service.php';
            require_once SMARTCO_ELEMENTOR_INCLUDES . '/widgets/smartco-team-slider.php';
            require_once SMARTCO_ELEMENTOR_INCLUDES . '/widgets/smartco-testimonial-slider.php';
            require_once SMARTCO_ELEMENTOR_INCLUDES . '/widgets/smartco-blogs.php';
            require_once SMARTCO_ELEMENTOR_INCLUDES . '/widgets/smartco-contact.php';
            require_once SMARTCO_ELEMENTOR_INCLUDES . '/widgets/smartco-project-slider.php';
            require_once SMARTCO_ELEMENTOR_INCLUDES . '/widgets/smartco-service-slider.php';
            require_once SMARTCO_ELEMENTOR_INCLUDES . '/widgets/smartco-brand.php';
            require_once SMARTCO_ELEMENTOR_INCLUDES . '/widgets/smartco-contact-details.php';

        }
    }

}
