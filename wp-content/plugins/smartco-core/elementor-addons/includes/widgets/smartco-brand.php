<?php
  use Elementor\Utils;

  class SmartcoBrand extends \Elementor\Widget_Base {

    public function get_name() {
    return 'SmartcoBrand';
  }

  public function get_title() {
    return esc_html__( 'Brands', 'smartco-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'smartco' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
         'content',
         [
           'label' => __( 'Content', 'smartco-core' ),
         ]
      );
            $this->add_control(
              'extra_class',
              [
                'label' => __( 'Extra Class', 'diaco' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( '', 'smartco-core' ),
              ]
            );
          $repeater = new \Elementor\Repeater();
          $repeater->add_control(
            'title',
            [
              'label' => __( 'Title', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
            ]
          );
          $repeater->add_control(
            'brand_image',
            [
              'label' => __( 'Brand Image', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::MEDIA,
              'default' => [
                        'url' => Utils::get_placeholder_image_src(),
                    ],
              
            ]
          );
      $this->end_controls_section();

      $this->start_controls_section(
        'brand_list',
        [
          'label' => __( 'Brand List', 'smartco-core' ),
        ]
      );
      $this->add_control(
        'items1',
        [
          'label' => __( 'Repeater List', 'smartco-core' ),
          'type' => \Elementor\Controls_Manager::REPEATER,
          'fields' => $repeater->get_controls(),
          'default' => [
            [
              'list_title' => __( 'Title #1', 'smartco-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'smartco-core' ),
            ],
            [
              'list_title' => __( 'Title #2', 'smartco-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'smartco-core' ),
            ],
          ],
        ]
      );
  
      $this->end_controls_section();
  
    }    
    protected function render() {
      $settings =  $this->get_settings_for_display(); 
      $extra_class = $settings["extra_class"]; 
      
?>


    <div class="partner-brand <?php echo esc_attr($extra_class);?>">
        <div class="row">
        <?php 
            foreach($settings["items1"] as $item){ 
              $title = $item["title"]; 
              $brand_image = $item[ 'brand_image']['url'];
              ?>      
            <!-- single-expertise-->
            <div class="col-lg-2 col-md-3 col-sm-6">
                <div class="brand-item">
                    <img src="<?php echo esc_url($brand_image);?>" alt="brand">
                </div>
            </div>
            <?php } ?>
        </div>
    </div>    

 <?php 
    }
  
    protected function _content_template() {
      
    }
  }

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \SmartcoBrand() );