<?php
  use Elementor\Utils;

  class SmartcoContactDatails extends \Elementor\Widget_Base {

    public function get_name() {
    return 'SmartcoContactDatails';
  }

  public function get_title() {
    return esc_html__( 'Contact Details', 'smartco-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'smartco' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
         'contact_details',
         [
           'label' => __( 'Contact Details', 'smartco-core' ),
         ]
      );
            $this->add_control(
              'title',
              [
                'label' => __( 'Title', 'diaco' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( '', 'smartco-core' ),
              ]
            );
            $this->add_control(
              'content',
              [
                'label' => __( 'Content', 'diaco' ),
                'type' => \Elementor\Controls_Manager::TEXTAREA,
                'default' => __( '', 'smartco-core' ),
              ]
            );
            $this->add_control(
              'address',
              [
                'label' => __( 'Address', 'diaco' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( '36th Ave. Street, Astoria, New Yourk city', 'smartco-core' ),
              ]
            );
            $this->add_control(
              'phone',
              [
                'label' => __( 'Phone', 'diaco' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( '+880 123 456 7823', 'smartco-core' ),
              ]
            );
            $this->add_control(
              'email',
              [
                'label' => __( 'Email', 'diaco' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( 'info@example.com', 'smartco-core' ),
              ]
            );
          $this->add_control(
            'map_iframe',
            [
              'label' => __( 'Map Iframe', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::TEXTAREA,
            ]
          );

      $this->end_controls_section();
  
    }    
    protected function render() {
      $settings =  $this->get_settings_for_display(); 
      $title = $settings["title"]; 
      $content = $settings["content"]; 
      $address = $settings["address"]; 
      $phone = $settings["phone"];
      $email = $settings["email"];
      $map_iframe = $settings["map_iframe"];
      
?>
    <div class="primary-contact">
        <div class="contact-us-heading">
            <h2><?php echo esc_html($title);?></h2>
        </div>
        <p><?php echo wp_kses_post($content);?></p>
        <p class="address"><i class="fa fa-map-marker mr-3"></i><?php echo wp_kses_post($address);?></p>
        <p class="phone"><i class="fa fa-phone mr-3"></i><?php echo wp_kses_post($phone);?></p>
        <p class="website"><i class="fa fa-envelope mr-3"></i><?php echo wp_kses_post($email);?></p>
        <a class="launch-map" data-toggle="modal" data-target="#exampleModal"><?php esc_html_e('Find Us','smartco-core');?><i class="fa fa-map-marker"></i></a>
    </div>

    <div class="modal fade contact-modal" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true" style="padding-right: 0;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <?php echo $map_iframe;?>
                </div>

            </div>
        </div>
    </div>
 <?php 
    }
  
    protected function _content_template() {
      
    }
  }

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \SmartcoContactDatails() );