<?php
  use Elementor\Utils;

  class SmartcoServiceSlider extends \Elementor\Widget_Base {

    public function get_name() {
    return 'SmartcoServiceSlider';
  }

  public function get_title() {
    return esc_html__( 'Service Slider', 'smartco-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'smartco' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
         'content',
         [
           'label' => __( 'Content', 'smartco-core' ),
         ]
      );
            $this->add_control(
              'extra_class',
              [
                'label' => __( 'Extra Class', 'diaco' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( '', 'smartco-core' ),
              ]
            );
          $repeater = new \Elementor\Repeater();
          $repeater->add_control(
            'title',
            [
              'label' => __( 'Title', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
            ]
          );
          $repeater->add_control(
            'service_image',
            [
              'label' => __( 'Project Image', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::MEDIA,
              'default' => [
                        'url' => Utils::get_placeholder_image_src(),
                    ],
              
            ]
          );
      $this->end_controls_section();

      $this->start_controls_section(
        'service_list',
        [
          'label' => __( 'Service List', 'smartco-core' ),
        ]
      );
      $this->add_control(
        'items1',
        [
          'label' => __( 'Repeater List', 'smartco-core' ),
          'type' => \Elementor\Controls_Manager::REPEATER,
          'fields' => $repeater->get_controls(),
          'default' => [
            [
              'list_title' => __( 'Title #1', 'smartco-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'smartco-core' ),
            ],
            [
              'list_title' => __( 'Title #2', 'smartco-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'smartco-core' ),
            ],
          ],
        ]
      );
  
      $this->end_controls_section();
  
    }    
    protected function render() {
      $settings =  $this->get_settings_for_display(); 
      $extra_class = $settings["extra_class"]; 
      
?>

    <div class="row <?php echo esc_attr($extra_class);?>">
        <div class="dot-list owl-carousel">
        <?php 
            foreach($settings["items1"] as $item){ 
              $title = $item["title"]; 
              $service_image = $item[ 'service_image']['url'];
              ?>      
            <!-- single-expertise-->
            <div class="col-xl-12 col-md-12 col-sm-12 col-xs-12">
                <div class="expertise-item">
                    <img src="<?php echo esc_url($service_image);?>" alt="">
                    <div class="expertise-text">
                        <h5><?php echo $title;?></h5>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>    

 <?php 
    }
  
    protected function _content_template() {
      
    }
  }

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \SmartcoServiceSlider() );