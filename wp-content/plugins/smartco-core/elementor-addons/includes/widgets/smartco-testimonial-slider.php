<?php
  use Elementor\Utils;

  class SmartcoTestimonialSlider extends \Elementor\Widget_Base {

    public function get_name() {
    return 'SmartcoTestimonialSlider';
  }

  public function get_title() {
    return esc_html__( 'Testimonial Slider', 'smartco-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'smartco' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
         'content',
         [
           'label' => __( 'Content', 'smartco-core' ),
         ]
      );
            $this->add_control(
              'style',
              [
                'label' => __( 'Slider', 'smartco-core' ),
                'type' => \Elementor\Controls_Manager::SELECT,
                'default' => 'on',
                'options' => [
                  'on'  => __( 'On', 'smartco-core' ),
                  'off' => __( 'Off', 'smartco-core' ),
      
                ],
      
              ]
            );
            $this->add_control(
              'extra_class',
              [
                'label' => __( 'Extra Class', 'smartco' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( '', 'smartco-core' ),
              ]
            );
          $repeater = new \Elementor\Repeater();
          $repeater->add_control(
            'name',
            [
              'label' => __( 'Name', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
              'default' => __( 'john doe', 'smartco-core' ),
            ]
          );
          $repeater->add_control(
            'designation',
            [
              'label' => __( 'Designation', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
              'default' => __( 'CEO of abc news', 'smartco-core' ),
            ]
          );
          $repeater->add_control(
            'image',
            [
              'label' => __( 'Image', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::MEDIA,
              'default' => [
                        'url' => Utils::get_placeholder_image_src(),
                    ],
              
            ]
          );
          $repeater->add_control(
            'content',
            [
              'label' => __( 'Content', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::TEXTAREA,
            ]
          );
      $this->end_controls_section();

      $this->start_controls_section(
        'testimonial_list',
        [
          'label' => __( 'Testimonial List', 'smartco-core' ),
        ]
      );
      $this->add_control(
        'items1',
        [
          'label' => __( 'Repeater List', 'smartco-core' ),
          'type' => \Elementor\Controls_Manager::REPEATER,
          'fields' => $repeater->get_controls(),
          'default' => [
            [
              'list_title' => __( 'Title #1', 'smartco-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'smartco-core' ),
            ],
            [
              'list_title' => __( 'Title #2', 'smartco-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'smartco-core' ),
            ],
          ],
        ]
      );
  
      $this->end_controls_section();
  
    }    
    protected function render() {
      $settings =  $this->get_settings_for_display(); 
      $style = $settings["style"]; 
      $extra_class = $settings["extra_class"]; 
      
?>
    <?php 
      if($style == 'off'){ ?>
        <?php foreach($settings["items1"] as $item){ 
          $name = $item["name"]; 
          $designation = $item["designation"]; 
          $content = $item["content"]; 
          $image = $item[ 'image']['url']; 
          ?>
        <!-- single-features-->
            <div class="single-testimonial project-testimonial <?php echo esc_attr($extra_class);?>">
                <div class="client-img">
                    <img src="<?php echo esc_url($image);?>" alt="">
                </div>
                <div class="testimonial-content">
                    <h5><?php echo esc_html($name);?></h5>
                    <h6><?php echo esc_html($designation);?></h6>
                    <p><?php echo esc_html($content);?></p>
                </div>
            </div>
        <?php } ?>  
<?php
      }else{
    ?>
    <div class="row <?php echo esc_attr($extra_class);?>">
        <div class="dot-list owl-carousel">
        <?php foreach($settings["items1"] as $item){ 
              $name = $item["name"]; 
              $designation = $item["designation"]; 
              $content = $item["content"]; 
              $image = $item[ 'image']['url']; 
              ?>
            <!-- single-features-->
            <div class="col-xl-12 col-md-12 col-sm-12 col-xs-12">
                <div class="single-testimonial">
                    <div class="client-img">
                        <img src="<?php echo esc_url($image);?>" alt="">
                    </div>
                    <div class="testimonial-content">
                        <h5><?php echo esc_html($name);?></h5>
                        <h6><?php echo esc_html($designation);?></h6>
                        <p><?php echo esc_html($content);?></p>
                    </div>
                </div>
            </div>
            <?php } ?>      
        </div>
    </div>


 <?php 
      }
    }
  
    protected function _content_template() {
      
    }
  }

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \SmartcoTestimonialSlider() );