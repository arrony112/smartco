<?php
if (!defined('ABSPATH')) {
    exit;
}

use Elementor\Controls_Manager;
use Elementor\Plugin;
use Elementor\Utils;
use Elementor\Widget_Base;

class Blogs extends Widget_Base {

    public function get_name() {
        return 'blogs';
    }

    public function get_title() {
        return esc_html__('Blog', 'smartco-core');
    }

    public function get_icon() {
        return 'eicon-post';
    }

    public function get_categories() {
        return ['smartco'];
    }

    private function get_blog_categories() {
        $options = array();
        $taxonomy = 'category';
        if (!empty($taxonomy)) {
            $terms = get_terms(
                    array(
                        'parent' => 0,
                        'taxonomy' => $taxonomy,
                        'hide_empty' => false,
                    )
            );
            if (!empty($terms)) {
                foreach ($terms as $term) {
                    if (isset($term)) {
                        $options[''] = 'Select';
                        if (isset($term->slug) && isset($term->name)) {
                            $options[$term->slug] = $term->name;
                        }
                    }
                }
            }
        }
        return $options;
    }

    protected function _register_controls() {

        $this->start_controls_section(
                'section_blogs', [
            'label' => esc_html__('Blogs', 'smartco-core'),
                ]
        );

        $this->add_control(
                'category_id', [
            'type' => \Elementor\Controls_Manager::SELECT,
            'label' => esc_html__('Category', 'smartco-core'),
            'options' => $this->get_blog_categories()
                ]
        );

        $this->add_control(
                'number', [
            'label' => esc_html__('Number of Post', 'smartco-core'),
            'type' => Controls_Manager::TEXT,
            'default' => 2
                ]
        );

        $this->add_control(
                'order_by', [
            'label' => esc_html__('Order By', 'smartco-core'),
            'type' => Controls_Manager::SELECT,
            'default' => 'date',
            'options' => [
                'date' => esc_html__('Date', 'smartco-core'),
                'ID' => esc_html__('ID', 'smartco-core'),
                'author' => esc_html__('Author', 'smartco-core'),
                'title' => esc_html__('Title', 'smartco-core'),
                'modified' => esc_html__('Modified', 'smartco-core'),
                'rand' => esc_html__('Random', 'smartco-core'),
                'comment_count' => esc_html__('Comment count', 'smartco-core'),
                'menu_order' => esc_html__('Menu order', 'smartco-core')
            ]
                ]
        );

        $this->add_control(
                'order', [
            'label' => esc_html__('Order', 'smartco-core'),
            'type' => Controls_Manager::SELECT,
            'default' => 'desc',
            'options' => [
                'desc' => esc_html__('DESC', 'smartco-core'),
                'asc' => esc_html__('ASC', 'smartco-core')
            ]
                ]
        );

        $this->add_control(
                'extra_class', [
            'label' => esc_html__('Extra Class', 'smartco-core'),
            'type' => Controls_Manager::TEXT
                ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings();
        $extra_class = $settings['extra_class'];
        $posts_per_page = $settings['number'];

 
        $order_by = $settings['order_by'];
        $order = $settings['order'];
        $pg_num = get_query_var('paged') ? get_query_var('paged') : 1;
        $args = array(
            'post_type' => array('post'),
            'post_status' => array('publish'),
            'nopaging' => false,
            'paged' => $pg_num,
            'posts_per_page' => $posts_per_page,
            'category_name' => $settings['category_id'],
            'orderby' => $order_by,
            'order' => $order,
        );
        $query = new WP_Query($args);

        ?>

  <!-- news-section -->
    <div class="row <?php echo $extra_class; ?>">
        <?php

        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
        ?>  
        <!-- single-blog-->
        <div class="col-xl-4 col-lg-4 col-md-4 single-blog-mb">
            <div class="single-blog">
                <!-- blog-image-->
                <div class="blog-image">
                    <?php 
                        echo the_post_thumbnail();
                    ?>
                    <div class="blog-tag">
                        <?php echo smartco_categories() ?>
                    </div>
                </div>
                <!-- blog-content-->
                <div class="blog-content">
                    <div class="blog-content-heading">
                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> </h4>
                    </div>
                    <div class="blog-date">
                        <span><?php the_time( get_option( 'date_format' ) ); ?></span>
                    </div>
                
                 <?php  echo wp_trim_words( get_the_content(), 10 );?>
                    <a href="<?php the_permalink(); ?>" class="blog-readmore-btn"> <?php esc_html_e('read more','smartco-core');?></a>
                </div>
            </div>
        </div>
        <?php
             
            }
            wp_reset_postdata();
        }
        ?>   
    </div>


    <!-- news-section end -->
        <?php
    }

    protected function content_template() {
        
    }

}

Plugin::instance()->widgets_manager->register_widget_type(new Blogs());
