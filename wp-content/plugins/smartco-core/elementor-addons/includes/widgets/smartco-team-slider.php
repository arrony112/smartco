<?php
  use Elementor\Utils;

  class SmartcoTeamSlider extends \Elementor\Widget_Base {

    public function get_name() {
    return 'SmartcoTeamSlider';
  }

  public function get_title() {
    return esc_html__( 'Team Slider', 'smartco-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'smartco' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
         'content',
         [
           'label' => __( 'Content', 'smartco-core' ),
         ]
      );
            $this->add_control(
              'extra_class',
              [
                'label' => __( 'Extra Class', 'smartco' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( '', 'smartco-core' ),
              ]
            );
          $repeater = new \Elementor\Repeater();
          $repeater->add_control(
            'name',
            [
              'label' => __( 'Name', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
              'default' => __( 'john doe', 'smartco-core' ),
            ]
          );
          $repeater->add_control(
            'designation',
            [
              'label' => __( 'Designation', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
              'default' => __( 'Web Designer', 'smartco-core' ),
            ]
          );
          $repeater->add_control(
            'image',
            [
              'label' => __( 'Image', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::MEDIA,
              'default' => [
                        'url' => Utils::get_placeholder_image_src(),
                    ],
              
            ]
          );
          $repeater->add_control(
            'facebook_url',
            [
                'label' => __( 'Facebook Url', 'smartco' ),
                'type' => \Elementor\Controls_Manager::URL,
                'placeholder' => __( 'https://your-link.com', 'diaco-core' ),
                'show_external' => true,
                'default' => [
                  'url' => '',
                  'is_external' => true,
                  'nofollow' => true,
                ],
                
              ]
            );
            $repeater->add_control(
              'twitter_url',
              [
                  'label' => __( 'Twitter Url', 'smartco' ),
                  'type' => \Elementor\Controls_Manager::URL,
                  'placeholder' => __( 'https://your-link.com', 'diaco-core' ),
                  'show_external' => true,
                  'default' => [
                    'url' => '',
                    'is_external' => true,
                    'nofollow' => true,
                  ],
                  
                ]
              );
              $repeater->add_control(
                'google_plus_url',
                [
                    'label' => __( 'Google Plus Url', 'smartco' ),
                    'type' => \Elementor\Controls_Manager::URL,
                    'placeholder' => __( 'https://your-link.com', 'diaco-core' ),
                    'show_external' => true,
                    'default' => [
                      'url' => '',
                      'is_external' => true,
                      'nofollow' => true,
                    ],
                    
                  ]
                );
                $repeater->add_control(
                  'pinterest_url',
                  [
                      'label' => __( 'Pinterest Url', 'smartco' ),
                      'type' => \Elementor\Controls_Manager::URL,
                      'placeholder' => __( 'https://your-link.com', 'diaco-core' ),
                      'show_external' => true,
                      'default' => [
                        'url' => '',
                        'is_external' => true,
                        'nofollow' => true,
                      ],
                      
                    ]
                  );
      $this->end_controls_section();

      $this->start_controls_section(
        'team_member_list',
        [
          'label' => __( 'Team Member List', 'smartco-core' ),
        ]
      );
      $this->add_control(
        'items1',
        [
          'label' => __( 'Repeater List', 'smartco-core' ),
          'type' => \Elementor\Controls_Manager::REPEATER,
          'fields' => $repeater->get_controls(),
          'default' => [
            [
              'list_title' => __( 'Title #1', 'smartco-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'smartco-core' ),
            ],
            [
              'list_title' => __( 'Title #2', 'smartco-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'smartco-core' ),
            ],
          ],
        ]
      );
  
      $this->end_controls_section();
  
    }    
    protected function render() {
      $settings =  $this->get_settings_for_display(); 
      $extra_class = $settings["extra_class"]; 
      
?>

    <div class="row <?php echo esc_attr($extra_class);?>">
        <div class="dot-list owl-carousel">

        <?php foreach($settings["items1"] as $item){ 
              $name = $item["name"]; 
              $designation = $item["designation"]; 
              $image = $item[ 'image']['url']; 
              $facebook_url = $item[ 'facebook_url']['url']; 
              $twitter_url = $item[ 'twitter_url']['url']; 
              $google_plus_url = $item[ 'google_plus_url']['url']; 
              $pinterest_url = $item[ 'pinterest_url']['url']; 
              ?>
            <!-- single-team-member-->
            <div class="col-md-12">
                <div class="our-team">
                    <img src="<?php echo esc_url($image);?>" alt="">
                    <ul class="social">
                        <li><a href="<?php echo esc_url($facebook_url);?>" class="fab fa-facebook"></a></li>
                        <li><a href="<?php echo esc_url($twitter_url);?>" class="fab fa-twitter"></a></li>
                        <li><a href="<?php echo esc_url($google_plus_url);?>" class="fab fa-google-plus"></a></li>
                        <li><a href="<?php echo esc_url($pinterest_url);?>" class="fab fa-pinterest"></a></li>
                    </ul>
                    <!-- single-team-member-info-->
                    <div class="team-content">
                        <h3 class="title"><?php echo esc_html($name);?></h3>
                        <span class="post"><?php echo esc_html($designation);?></span>
                    </div>
                </div>
            </div>
            <?php } ?>
  
        </div>
    </div>


 <?php 
    }
  
    protected function _content_template() {
      
    }
  }

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \SmartcoTeamSlider() );