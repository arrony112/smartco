<?php
  use Elementor\Utils;

  class SmartcoContact extends \Elementor\Widget_Base {

    public function get_name() {
    return 'contact';
  }

  public function get_title() {
    return esc_html__( 'Contact', 'smartco-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'smartco' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
         'contact',
         [
           'label' => __( 'Contact', 'smartco-core' ),
         ]
      );
            $this->add_control(
              'extra_class',
              [
                'label' => __( 'Extra Class', 'diaco' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( '', 'smartco-core' ),
              ]
            );
          $this->add_control(
            'content',
            [
              'label' => __( 'Content', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::WYSIWYG,
            ]
          );
          $this->add_control(
            'contact_form',
            [
              'label' => __( 'Contact Form Shortcode', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
            ]
          );
          $this->add_control(
            'top_image',
            [
              'label' => __( 'Top Image', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::MEDIA,
              'default' => [
                        'url' => Utils::get_placeholder_image_src(),
                    ],
              
            ]
          );
          $this->add_control(
            'bottom_image',
            [
              'label' => __( 'Bottom Image', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::MEDIA,
              'default' => [
                        'url' => Utils::get_placeholder_image_src(),
                    ],
              
            ]
          );

      $this->end_controls_section();
  
    }    
    protected function render() {
      $settings =  $this->get_settings_for_display(); 
      $extra_class = $settings["extra_class"]; 
      $content = $settings["content"]; 
      $contact_form = $settings["contact_form"]; 
      $top_image = $settings["top_image"]['url']; 
      $bottom_image = $settings["bottom_image"]['url']; 
      
?>
    <section id="contact">
        <div class="contact-area">
            <div class="container">
                <div class="row">
                    <!-- services content-->
                    <div class="col-xl-7 col-lg-7 col-md-7">
                        <div class="contact-content">
                            <!-- contact-icon-->
                            <?php if($top_image){?>
                            <div class="contact-icon" data-aos="fade-up" data-aos-duration="1200" data-aos-easing="ease-in-sine" data-aos-anchor-placement="top-bottom">
                                <img src="<?php echo esc_url($top_image);?>" alt="animate image">
                            </div>
                            <?php } ?>
                            <!-- section heading-->
                            <?php echo wp_kses_post($content);?>
                        </div>
                    </div>
                    <!-- contact form area-->
                    <div class="col-xl-5 col-lg-5 col-md-5">
                        <div class="contact-box">
                            <div class="row">
                            <?php echo do_shortcode($contact_form);?>
                            </div>
                        </div>
                    </div>
                    <!-- footer-icon-->
                    <?php if($bottom_image){?>
                    <div class="col-xl-12" data-aos="fade-up" data-aos-duration="1200" data-aos-easing="ease-in-sine" data-aos-anchor-placement="top-bottom">
                        <div class="footer-icon">
                            <img src="<?php echo esc_url($bottom_image);?>" alt="animate image">
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>

        </div>
    </section>


 <?php 
    }
  
    protected function _content_template() {
      
    }
  }

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \SmartcoContact() );