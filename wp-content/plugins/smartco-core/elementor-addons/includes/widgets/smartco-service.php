<?php
  use Elementor\Utils;

  class SmartcoService extends \Elementor\Widget_Base {

    public function get_name() {
    return 'Service';
  }

  public function get_title() {
    return esc_html__( 'Service', 'smartco-core' );
  }

  public function get_icon() {
    return '';
  }

   public function get_categories() {
    return [ 'smartco' ];
  }
  
    protected function _register_controls() {

      $this->start_controls_section(
         'content',
         [
           'label' => __( 'Content', 'smartco-core' ),
         ]
      );
            $this->add_control(
              'extra_class',
              [
                'label' => __( 'Extra Class', 'smartco-core' ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __( '', 'smartco-core' ),
              ]
            );
          $repeater = new \Elementor\Repeater();
          $repeater->add_control(
            'title',
            [
              'label' => __( 'Title', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
            ]
          );
          $repeater->add_control(
            'desc',
            [
              'label' => __( 'Content', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::TEXTAREA,
            ]
          );
          $repeater->add_control(
            'icon_image',
            [
              'label' => __( 'Icon Image', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::MEDIA,
              'default' => [
                        'url' => Utils::get_placeholder_image_src(),
                    ],
              
            ]
          );
          $repeater->add_control(
            'add_class',
            [
              'label' => __( 'Add Class', 'smartco-core' ),
              'type' => \Elementor\Controls_Manager::TEXT,
              'default' => __( 'margin-boottom30' ),
            ]
          );
      $this->end_controls_section();

      $this->start_controls_section(
        'service_list',
        [
          'label' => __( 'Service List', 'smartco-core' ),
        ]
      );
      $this->add_control(
        'items1',
        [
          'label' => __( 'Repeater List', 'smartco-core' ),
          'type' => \Elementor\Controls_Manager::REPEATER,
          'fields' => $repeater->get_controls(),
          'default' => [
            [
              'list_title' => __( 'Title #1', 'smartco-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'smartco-core' ),
            ],
            [
              'list_title' => __( 'Title #2', 'smartco-core' ),
              'list_content' => __( 'Item content. Click the edit button to change this text.', 'smartco-core' ),
            ],
          ],
        ]
      );
  
      $this->end_controls_section();
  
    }    
    protected function render() {
      $settings =  $this->get_settings_for_display(); 
      $extra_class = $settings["extra_class"]; 
      
?>      
    <div class="features-box <?php echo esc_attr($extra_class);?>">
        <div class="row">
        <?php foreach($settings["items1"] as $item){ 
              $title = $item["title"]; 
              $desc = $item["desc"]; 
              $add_class = $item["add_class"]; 
              $icon_image = $item[ 'icon_image']['url']; 
              ?>
            <!-- single-features-->
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 <?php echo esc_attr($add_class);?> " data-aos="fade-up" data-aos-duration="1200" data-aos-easing="ease-in-sine" data-aos-anchor-placement="top-bottom">
                <div class="single-features">
                    <img src="<?php echo esc_url($icon_image);?>" alt="Service Icon">
                    <h4><?php echo $title;?></h4>
                    <p><?php echo $desc;?></p>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>




 <?php 
    }
  
    protected function _content_template() {
      
    }
  }

\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \SmartcoService() );