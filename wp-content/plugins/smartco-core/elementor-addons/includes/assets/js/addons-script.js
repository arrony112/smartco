(function ($) {
    "use strict";



    var SmartcoProjectSlider = function ($scope, $) {
        // Project Carousel
        $('.dot-list').owlCarousel({
            loop:true,
            autoplayHoverPause:true,
            autoplay:true,
            dots: true,
            smartSpeed:1000,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                1000:{
                    items:3
                }
        }});

    }

    var Service = function ($scope, $) {
        AOS.init();
    }




    $(window).on('elementor/frontend/init', function () {
        elementorFrontend.hooks.addAction('frontend/element_ready/SmartcoProjectSlider.default', SmartcoProjectSlider);
        elementorFrontend.hooks.addAction('frontend/element_ready/SmartcoServiceSlider.default', SmartcoProjectSlider);
        elementorFrontend.hooks.addAction('frontend/element_ready/SmartcoTeamSlider.default', SmartcoProjectSlider);
        elementorFrontend.hooks.addAction('frontend/element_ready/SmartcoTestimonialSlider.default', SmartcoProjectSlider);
        elementorFrontend.hooks.addAction('frontend/element_ready/Service.default', Service);
    });
})(jQuery);