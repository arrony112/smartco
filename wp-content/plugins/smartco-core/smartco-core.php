<?php
/*
  Plugin Name: Smartco Core
  Plugin URI: http://themepuller.com/
  Description: Smartco Core theme functions and library files.
  Version: 1.0
  Author: themepuller
  Author URI: http://themepuller.com/
  License: GPLv2 or later
  Text Domain: smartco-core
  Domain Path: /languages/
 */

define('PLUGIN_DIR', dirname(__FILE__) . '/');

if (!defined('SMARTCO_CORE_PLUGIN_URI')) {
    define('SMARTCO_CORE_PLUGIN_URI', plugin_dir_url(__FILE__));
}

if (!class_exists('SmartcoCore')) {

    class SmartcoCore {

        public static $plugindir, $pluginurl;

        function __construct() {

            SmartcoCore::$plugindir = dirname(__FILE__);

            SmartcoCore::$pluginurl = plugins_url('', __FILE__);
        }


    }

    $smartcoCore = new SmartcoCore();


    require_once( SmartcoCore::$plugindir . "/widgets/smartco-latest-post-widget.php" );
    require_once( SmartcoCore::$plugindir . "/widgets/smartco-social-link-widget.php" );
    require_once( SmartcoCore::$plugindir . "/widgets/smartco-contact-widget.php" );
    require_once( SmartcoCore::$plugindir . "/meta-box/page-meta.php" );


    require_once( SmartcoCore::$plugindir . "/elementor-addons/smartco-elementor.php" );

}

function smartco_core_load_textdomain() {
    load_plugin_textdomain('smartco-core', false, dirname(__FILE__) . "/languages");
}


add_action('plugins_loaded', 'smartco_core_load_textdomain');
