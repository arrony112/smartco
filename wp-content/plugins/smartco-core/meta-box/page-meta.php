<?php

add_filter('rwmb_meta_boxes', 'smartco_register_framework_post_meta_box');

/**
 * Register meta boxes
 *
 * Remember to change "your_prefix" to actual prefix in your project
 *
 * @return void
 */
function smartco_register_framework_post_meta_box($meta_boxes) {

    global $wp_registered_sidebars;


    $sidebars = array(
        '0' => esc_html__('Default widget', 'smartco-core')
    );

    foreach ($wp_registered_sidebars as $key => $value) {
        $sidebars[$key] = $value['name'];
    }


    /**
     * prefix of meta keys (optional)
     * Use underscore (_) at the beginning to make keys hidden
     * Alt.: You also can make prefix empty to disable it
     */
    // Better has an underscore as last sign
    $prefix = 'smartco';


    $posts_page = get_option('page_for_posts');

    if (!isset($_GET['post']) || intval($_GET['post']) != $posts_page) {

        $meta_boxes[] = array(
            'id' => $prefix . '_page_meta_box',
            'title' => esc_html__('Page Design Settings', 'smartco-core'),
            'pages' => array(
                'page',
            ),
            'context' => 'normal',
            'priority' => 'core',
            'fields' => array(
                array(
                    'name' => esc_html__('Title', 'smartco-core'),
                    'id' => "{$prefix}_title",
                    'desc' => '',
                    'type' => 'text',
                ),
                array(
                    'name' => esc_html__('Subtitle', 'smartco-core'),
                    'id' => "{$prefix}_subtitle",
                    'desc' => '',
                    'type' => 'textarea',
                ),
                array(
                    'name' => esc_html__('Button Text', 'smartco-core'),
                    'id' => "{$prefix}_button_text",
                    'desc' => '',
                    'type' => 'text',
                ),
                array(
                    'name' => esc_html__('Button Link', 'smartco-core'),
                    'id' => "{$prefix}_button_link",
                    'desc' => '',
                    'type' => 'text',
                ),
                array(
                    'id' => "{$prefix}_page_style",
                    'name' => esc_html__('Header Style', 'smartco-core'),
                    'desc' => '',
                    'type' => 'select',
                    'std' => "one",
                    'options' => array(
                        'one' => 'One',
                        'two' => 'Two',
                    ),
                    'allowClear' => true,
                    'placeholder' => esc_html__('Select', 'smartco-core'),
                ),
            )
        );
    }


    return $meta_boxes;
}
